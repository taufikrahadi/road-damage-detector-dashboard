import { Backdrop, CircularProgress } from '@mui/material'

export interface LoadingProps {
  show: boolean
}

const Loading = ({ show }: LoadingProps) => {
  return (
    <Backdrop
      sx={{
        color: '#fff',
        zIndex: theme => theme.zIndex.drawer + 1
      }}
      open={show}
    >
      <CircularProgress color='inherit' />
    </Backdrop>
  )
}

export default Loading
