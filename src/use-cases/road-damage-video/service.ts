const AI_SERVICE_URL = process.env.AI_SERVICE_URL

export const postRoadDamageVideo = async (body: { video: string; timestamp: string }) => {
  const result = await fetch(AI_SERVICE_URL + '/process-ai', {
    method: 'POST',
    body: JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json'
    }
  })

  return await result.json()
}
