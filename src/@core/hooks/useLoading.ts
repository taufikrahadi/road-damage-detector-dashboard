import nProgress from 'nprogress'
import { useState } from 'react'

export const useLoading = (defaultValue: boolean) => {
  const [loading, setLoading] = useState(defaultValue)

  const startLoading = () => {
    setLoading(true)
    nProgress.start()
  }
  const stopLoading = () => {
    setLoading(false)
    nProgress.done()
  }

  return [loading, startLoading, stopLoading] as const
}
