export const MockAI = async ({ video, timestamp }: { video: string; timestamp: string }) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve({
        video: `detection/1698077624174/ai-video/testing.mp4`,
        data: [
          {
            timestamp: '00:06:01',
            coordinate: '',
            capture: 'https://i0.wp.com/strong-indonesia.com/wp-content/uploads/2017/11/aspal-retak.jpg',
            damages: ['Retak Melintang']
          },
          {
            timestamp: '00:06:01',
            coordinate: '',
            capture: 'https://i0.wp.com/strong-indonesia.com/wp-content/uploads/2017/11/aspal-retak.jpg',
            damages: ['Retak Buaya']
          },
          {
            timestamp: '00:06:01',
            coordinate: '',
            capture: 'https://i0.wp.com/strong-indonesia.com/wp-content/uploads/2017/11/aspal-retak.jpg',
            damages: ['Lubang']
          },
          {
            timestamp: '00:06:01',
            coordinate: '',
            capture: 'https://i0.wp.com/strong-indonesia.com/wp-content/uploads/2017/11/aspal-retak.jpg',
            damages: ['Retak Memanjang', 'Lubang', 'Retak Buaya']
          },
          {
            timestamp: '00:06:01',
            coordinate: '',
            capture: 'https://i0.wp.com/strong-indonesia.com/wp-content/uploads/2017/11/aspal-retak.jpg',
            damages: ['Retak Memanjang', 'Lubang', 'Retak Buaya', 'Retak Melintang']
          }
        ]
      })
    }, 1500)
  })
}
