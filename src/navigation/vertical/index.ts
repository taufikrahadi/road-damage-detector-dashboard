// ** Icon imports
import HomeOutline from 'mdi-material-ui/HomeOutline'
import AlertCircleOutline from 'mdi-material-ui/AlertCircleOutline'
import AccountDetails from 'mdi-material-ui/AccountDetails'

// ** Type import
import { VerticalNavItemsType } from 'src/@core/layouts/types'

const navigation = (): VerticalNavItemsType => {
  return [
    {
      title: 'Home',
      icon: HomeOutline,
      path: '/'
    },
    {
      title: 'Deteksi Kerusakan Jalan',
      icon: AlertCircleOutline,
      path: '/detector'
    },
    {
      title: 'About',
      icon: AccountDetails,
      path: '/about'
    }
  ]
}

export default navigation
