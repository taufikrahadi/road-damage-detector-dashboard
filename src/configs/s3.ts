import { S3 } from '@aws-sdk/client-s3'

export const s3 = new S3({
  credentials: {
    accessKeyId: String(process.env.AWS_S3_ACCESS_KEY),
    secretAccessKey: String(process.env.AWS_S3_SECRET_KEY)
  },
  region: String(process.env.AWS_S3_REGION)
})
