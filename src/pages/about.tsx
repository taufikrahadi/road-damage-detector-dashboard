import { Box, Card, CardContent, Grid, Typography } from '@mui/material'
import React from 'react'

function About() {
  return (
    <>
      <Grid container spacing={6}>
        <Grid item xs={12}>
          <Card>
            <CardContent>
              <Typography align='center' variant='h5'>
                Socamarga (Mata Jalan)
              </Typography>
              <br />
              <Typography variant='subtitle1' sx={{ lineHeight: '20px' }} align='center'>
                Nama SOCAMARGA diambil dari bahasa jawa yang terdiri dari kata soca yang berarti mata dan marga yang
                berarti jalan. Huruf pada SOCA juga merupakan akronim dari Smart Observation of Road Condition based
                Artificial Intelegen.
              </Typography>

              <br />
              <br />
              <br />
              <br />

              <Typography align='center'>Dibuat Oleh</Typography>
              <Typography align='center'>
                <strong>PT. GeoBIM Indonesia</strong>
              </Typography>
              <br />
              <Box sx={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                <img src='/images/logos/picture1.png' />
              </Box>

              <Typography variant='subtitle2' align='center'>
                Sleman, Yogyakarta
              </Typography>
              <Typography align='center' variant='subtitle2'>
                Tahun 2023
              </Typography>
            </CardContent>
          </Card>
        </Grid>
      </Grid>
    </>
  )
}

export default About
