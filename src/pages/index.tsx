import { Button, Card, CardContent, CardHeader, Grid, Typography } from '@mui/material'
import Link from 'next/link'
import React from 'react'
import { useLoading } from 'src/@core/hooks/useLoading'
import Loading from 'src/layouts/components/Loading'

function Dashboard() {
  const [loading, startLoading, stopLoading] = useLoading(false)

  return (
    <Grid container spacing={6}>
      <Loading show={loading} />

      <Grid item xs={12}>
        <Card>
          <CardContent>
            <Grid container spacing={5}>
              <Grid item xs={12}>
                <Typography variant='h5'>Deteksi Kerusakan Jalan - Socamarga</Typography>
                <br />
                <Typography variant='body2' align='justify'>
                  Selamat datang di platform revolusioner kami, "Socamarga"! Kami adalah solusi terdepan untuk
                  mendeteksi kerusakan jalan dengan cepat, mudah, dan akurat melalui video yang Anda unggah. Dengan
                  teknologi canggih kami, kami menggabungkan kecerdasan buatan dan visi komputer untuk memberikan
                  kontribusi nyata dalam pemeliharaan infrastruktur jalan yang aman dan berkualitas.
                </Typography>

                <br />
                <Typography variant='h6'>Cara penggunaan:</Typography>

                <Typography variant='body2'>
                  <ol>
                    <li>
                      Upload video dengan meng-klik tombol <strong>"Pilih Video"</strong>.
                    </li>
                    <li>
                      Upload file CSV atau GPX untuk data koordinat dengan meng-klik tombol{' '}
                      <strong>"Pilih File Koordinat"</strong>.
                    </li>
                    <li>
                      Klik tombol <strong>"Upload"</strong> untuk melanjutkan proses deteksi dari file - file diatas.
                    </li>
                    <li>
                      Tunggu hingga proses pendeteksian selesai, dan data kerusakan jalan bisa dilihat di tabel bagian
                      bawah halaman.
                    </li>
                  </ol>
                  <strong>*</strong>{' '}
                  <small>Lama pemrosesan video tergantung dari ukuran file video yang diupload.</small>
                </Typography>
              </Grid>
              <Grid item xs={12}>
                <Button variant='contained' color='warning'>
                  <Link href='/detector'>Mulai</Link>
                </Button>
              </Grid>
            </Grid>
          </CardContent>
        </Card>
      </Grid>
    </Grid>
  )
}

export default Dashboard
