// ** MUI Imports
import Grid from '@mui/material/Grid'
import Link from '@mui/material/Link'
import Card from '@mui/material/Card'
import Typography from '@mui/material/Typography'
import CardHeader from '@mui/material/CardHeader'

// ** Demo Components Imports
import TableCustomized from 'src/views/tables/TableCustomized'
import {
  Button,
  CardActions,
  CardContent,
  FormControl,
  InputLabel,
  MenuItem,
  Paper,
  Select,
  SelectChangeEvent,
  Table,
  TableBody,
  TableCell,
  TableCellProps,
  TableContainer,
  TableHead,
  TableRow,
  TableRowProps,
  styled,
  tableCellClasses
} from '@mui/material'
import { FormEvent, useEffect, useState } from 'react'
import { useLoading } from 'src/@core/hooks/useLoading'
import Loading from 'src/layouts/components/Loading'
import { Upload } from '@aws-sdk/lib-storage'
import { CompleteMultipartUploadCommandOutput, GetObjectCommand, S3 as S3Lagi } from '@aws-sdk/client-s3'
import { S3 } from 'aws-sdk'
import { getSignedUrl } from '@aws-sdk/s3-request-presigner'
import { useRouter } from 'next/router'
import ReactPlayer from 'react-player'
import Toast from 'src/layouts/components/Toast'

export const getServerSideProps = () => {
  return {
    props: {
      accessKeyId: String(process.env.AWS_S3_ACCESS_KEY),
      secretAccessKey: String(process.env.AWS_S3_SECRET_KEY),
      region: String(process.env.AWS_S3_REGION),
      bucket: String(process.env.AWS_S3_BUCKET),
      apiUrl: String(process.env.API_URL)
    }
  }
}

const StyledTableCell = styled(TableCell)<TableCellProps>(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    color: theme.palette.common.white,
    backgroundColor: theme.palette.common.black
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14
  }
}))

const StyledTableRow = styled(TableRow)<TableRowProps>(({ theme }) => ({
  '&:nth-of-type(odd)': {
    backgroundColor: theme.palette.action.hover
  },

  // hide last border
  '&:last-of-type td, &:last-of-type th': {
    border: 0
  }
}))

const Detector = ({
  accessKeyId,
  secretAccessKey,
  region,
  bucket,
  apiUrl
}: {
  accessKeyId: string
  secretAccessKey: string
  region: string
  bucket: string
  apiUrl: string
}) => {
  const [results, setResults] = useState([])
  const [loading, startLoading, stopLoading] = useLoading(false)

  const [video, setVideo] = useState<File | null>(null)
  const [gpx, setGpx] = useState<File | null>(null)
  const [fileType, setFileType] = useState<string>('')
  const [upload, setUpload] = useState<S3.ManagedUpload | null>(null)
  const [videoUrl, setVideoUrl] = useState<string>('')
  const [gpxUrl, setGpxUrl] = useState<string>('')
  const [resultId, setResultId] = useState<number | null>(null)
  const [message, setMessage] = useState('')
  const s3 = new S3({
    accessKeyId,
    secretAccessKey,
    region
  })
  const getPresignedUrl = async (filename: string) => {
    const s3Client = new S3Lagi({
      credentials: {
        accessKeyId,
        secretAccessKey
      },
      region
    })
    const command = new GetObjectCommand({
      Bucket: 'road-detection',
      Key: filename
    })

    const url = await getSignedUrl(s3Client as any, command as any, { expiresIn: 60 * 60 * 24 * 7 })

    return url
  }
  const router = useRouter()

  const uploadGpx = async (timestamp: string) => {
    if (gpx !== null) {
      const buffer = Buffer.from((await gpx?.arrayBuffer()) as any)
      const Key = `detection/${timestamp}/gpx-file/${gpx?.name}`

      const upload = s3.upload({
        Bucket: bucket,
        Key,
        Body: buffer
      })

      await upload.promise()
      const url = await getPresignedUrl(Key)

      return url
    } else return null
  }

  const downloadVideo = async () => {
    startLoading()
    const response = await fetch(videoUrl)
    const blob = await response.blob()
    const url = window.URL.createObjectURL(blob)
    const a = document.createElement('a')

    a.href = url
    a.download = 'detection-result.mp4'
    a.style.display = 'none'
    document.body.appendChild(a)
    a.click()
    window.URL.revokeObjectURL(url)
    stopLoading()
  }

  const downloadCsv = async () => {
    try {
      const response = await fetch(`${apiUrl}/v2/detector/${resultId}`, {
        method: 'GET',
        headers: {
          'Content-Type': 'text/csv'
        }
      })

      if (response.ok) {
        const blob = await response.blob()
        const url = window.URL.createObjectURL(blob)

        const a = document.createElement('a')
        a.href = url
        a.download = 'detection-result.csv' // Set the desired file name
        a.style.display = 'none'

        document.body.appendChild(a)

        a.click()

        window.URL.revokeObjectURL(url)
        setMessage('Berhasil melakukan download CSV')
      }
    } catch (error) {
      console.error(error)
    }
  }

  const submitForm = async (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    try {
      startLoading()
      const buffer = Buffer.from((await video?.arrayBuffer()) as any)

      const timestamp = String(Date.now())

      const Key = `detection/${timestamp}/original-video/${video?.name}`

      const upload3 = s3.upload({
        Bucket: bucket,
        Key,
        Body: buffer
      })

      await upload3.promise()
      const url = await getPresignedUrl(Key)
      const gpx2 = await uploadGpx(timestamp)

      setIsUploadSuccess(true)

      const apiRes = await fetch(`${apiUrl}/v2/detector`, {
        method: 'POST',
        body: JSON.stringify({ video: url, timestamp, gpx: gpx2, file_type: fileType }),
        headers: new Headers({
          'Content-Type': 'application/json',
          Accept: 'application/json'
        })
      })
      const resultResponse = await apiRes.json()

      const aiUrl = await getPresignedUrl(resultResponse.savedVideo.processedVideo)
      setVideoUrl(aiUrl)
      setGpxUrl(gpx2 as any)
      setResultId(resultResponse.savedVideo.id)
      setResults(resultResponse.data)
    } catch (error: any) {
      router.push(`/500?message=${error.message}`)
    } finally {
      stopLoading()
    }
  }

  useEffect(() => {
    startLoading()
    setTimeout(() => {
      stopLoading()
    }, 1500)
  }, [])

  const [isUploadSuccess, setIsUploadSuccess] = useState(false)
  const [isSuccessDetection, setIsSuccessDetection] = useState(false)

  return (
    <Grid container spacing={6}>
      <Loading show={loading} />
      {/* 
      <Toast
        severity='success'
        show={isUploadSuccess}
        message='Video berhasil diupload, selanjutnya akan diproses menggunakan AI'
      /> */}

      <Grid item xs={12}>
        <form onSubmit={submitForm}>
          <Card>
            <CardContent>
              <Grid container spacing={5}>
                <Grid item xs={12}>
                  <Typography variant='h5'>Deteksi Kerusakan Jalan</Typography>
                  <Typography variant='body2'>
                    Upload video dan dapatkan hasil kerusakan jalan yang dikategorikan oleh AI{' '}
                  </Typography>
                </Grid>
                <Grid item xs={12}>
                  <label>Video</label>
                </Grid>
                <Grid item xs={12}>
                  <Button variant='contained' component='label' disabled={video !== null}>
                    {video === null ? `Pilih Video` : video?.name}
                    <input
                      type='file'
                      accept='video/*'
                      hidden
                      onChange={e => {
                        e.preventDefault()
                        const f = e.target.files![0]
                        setVideo(f)
                      }}
                    />
                  </Button>
                </Grid>

                <Grid item xs={12}>
                  <label>Tipe Informasi</label>
                </Grid>

                <Grid item xs={6}>
                  <FormControl fullWidth>
                    <InputLabel id='information'>Pilih Tipe Informasi</InputLabel>
                    <Select
                      labelId='information'
                      fullWidth
                      label='Pilih Tipe Informasi'
                      onChange={(e: SelectChangeEvent) => {
                        setFileType(e.target.value as string)
                      }}
                    >
                      <MenuItem value='longlat'>Latitude & Longitude</MenuItem>
                      <MenuItem value='utm'>UTM</MenuItem>
                    </Select>
                  </FormControl>
                </Grid>

                <Grid item xs={12}>
                  <label>File CSV / GPX</label>
                </Grid>
                <Grid item xs={12}>
                  <Button variant='contained' component='label' disabled={gpx !== null}>
                    {gpx === null ? `Pilih file yang memiliki informasi longitude & latitude atau UTM` : gpx?.name}
                    <input
                      type='file'
                      accept='.csv, .gpx'
                      hidden
                      onChange={e => {
                        e.preventDefault()
                        const f = e.target.files![0]
                        setGpx(f)
                      }}
                    />
                  </Button>
                </Grid>
              </Grid>
            </CardContent>
            <CardActions>
              <Button type='submit' variant='contained' disabled={video === null || gpx === null} color='primary'>
                Upload
              </Button>
            </CardActions>
          </Card>
        </form>
      </Grid>

      {results.length > 0 ? (
        <Grid item xs={12}>
          <Card>
            <CardHeader title='Hasil Deteksi' titleTypographyProps={{ variant: 'h6' }} />

            <Grid container spacing={6} justifyContent={'end'}>
              <Grid item xs={3}>
                <Button variant='contained' onClick={downloadCsv} color='primary'>
                  Download CSV
                </Button>
              </Grid>
              <Grid item xs={3}>
                <Button variant='contained' color='primary'>
                  <a href={videoUrl} target='_blank'>
                    Download Video
                  </a>
                </Button>
              </Grid>
            </Grid>

            <CardContent>
              <TableContainer style={{ overflowX: 'scroll' }} component={Paper}>
                <Table aria-label='customized table'>
                  <TableHead>
                    <TableRow>
                      <StyledTableCell>No</StyledTableCell>
                      <StyledTableCell align='right'>Timestamp</StyledTableCell>
                      <StyledTableCell align='right'>Koordinat</StyledTableCell>
                      <StyledTableCell align='right'>Kategori Kerusakan</StyledTableCell>
                      <StyledTableCell align='right'>Meter</StyledTableCell>
                      <StyledTableCell align='right'>Screen Capture</StyledTableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {(results as any[]).map((row, index) => (
                      <StyledTableRow key={index}>
                        <StyledTableCell component='th' scope='row'>
                          {index + 1}
                        </StyledTableCell>
                        <StyledTableCell align='right'>{row.timestamp}</StyledTableCell>
                        <StyledTableCell align='right'>
                          {row.latitude !== null && row.longitude !== null ? `${row.latitude} | ${row.longitude}` : ''}
                        </StyledTableCell>
                        <StyledTableCell align='left'>
                          <ul>
                            {row.category.map((c: any) => (
                              <li>{c}</li>
                            ))}
                          </ul>
                        </StyledTableCell>
                        <StyledTableCell align='right'>{row.meter}</StyledTableCell>
                        <StyledTableCell align='right'>
                          <img src={row.capture} height='200' />
                        </StyledTableCell>
                      </StyledTableRow>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
            </CardContent>
          </Card>
        </Grid>
      ) : (
        <></>
      )}
    </Grid>
  )
}

export default Detector
