import { CompleteMultipartUploadCommandOutput } from '@aws-sdk/client-s3'
import { Upload } from '@aws-sdk/lib-storage'
import { IncomingMessage, ServerResponse } from 'http'
import { s3 } from 'src/configs/s3'
import { NextApiRequest, NextApiResponse } from 'next/types'
import { postRoadDamageVideo } from 'src/use-cases/road-damage-video/service'

export const config = {
  api: {}
}

export const STORAGE_BUCKET = String(process.env.AWS_S3_BUCKET)

export const uploadFile = async (filename: string, buffer: Buffer) => {
  const upload = (await new Upload({
    client: s3,
    params: {
      Bucket: STORAGE_BUCKET,
      Key: filename,
      Body: buffer
    }
  }).done()) as CompleteMultipartUploadCommandOutput

  return upload.Location
}

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
  if (req.method === 'POST') {
    const result = await postRoadDamageVideo(req.body)
    res.status(201).json(result)
  }
}
